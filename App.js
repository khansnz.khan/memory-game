import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, FlatList} from 'react-native';

const generateCards = () => {
  const cards = [];
  const allCards = [
    {value: 'A'},
    {value: 'B'},
    {value: 'C'},
    {value: 'D'},
    {value: 'E'},
    {value: 'F'},
    {value: 'G'},
    {value: 'H'},
  ];

  for (let i = 0; i <= 7; i++) {
    cards.push({id: i * 2 - 1, value: allCards[i].value});
    cards.push({id: i * 2, value: allCards[i].value});
  }

  return cards;
};

const shuffleArray = array => {
  const shuffled = [...array];

  for (let i = shuffled.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [shuffled[i], shuffled[j]] = [shuffled[j], shuffled[i]];
  }

  return shuffled;
};

const Card = ({id, value, isRevealed, onPress}) => {
  const handlePress = () => {
    onPress(id);
  };

  return (
    <TouchableOpacity style={styles.card} onPress={handlePress}>
      {isRevealed ? (
        <Text style={styles.cardText}>{value}</Text>
      ) : (
        <Text style={styles.cardText}>?</Text>
      )}
    </TouchableOpacity>
  );
};

const App = () => {
  const [cards, setCards] = useState([]);
  const [selectedCards, setSelectedCards] = useState([]);
  const [attempts, setAttempts] = useState(0);
  const [matches, setMatches] = useState(0);

  useEffect(() => {
    const generatedCards = generateCards();
    const shuffledCards = shuffleArray(generatedCards);
    setCards(shuffledCards);
  }, [matches]);

  const handleResetPress = () => {
    const shuffledCards = shuffleArray(cards);
    setSelectedCards([]);
    setAttempts(0);
    setMatches(0);
    setCards(shuffledCards);
  };

  const handleCardPress = id => {
    const isCardRevealed = selectedCards.includes(id);

    if (isCardRevealed) {
      return;
    }

    if (selectedCards.length === 0) {
      setSelectedCards([id]);
      return;
    }

    const [cardId] = selectedCards;

    if (id === cardId) {
      return;
    }

    const newSelectedCards = [...selectedCards, id];
    setSelectedCards(newSelectedCards);

    if (newSelectedCards.length === 2) {
      setAttempts(attempts + 1);
      const [cardId1, cardId2] = newSelectedCards;
      const card1 = cards.find(card => card.id === cardId1);
      const card2 = cards.find(card => card.id === cardId2);

      if (card1.value === card2.value) {
        setMatches(matches + 1);
        setSelectedCards([]);
      } else {
        setTimeout(() => {
          setSelectedCards([cardId]);
          setTimeout(() => setSelectedCards([]), 200);
        }, 300);
      }
    }
  };

  const renderCardItem = ({item}) => {
    const isRevealed = selectedCards.includes(item.id);

    return (
      <Card
        id={item.id}
        value={item.value}
        isRevealed={isRevealed}
        onPress={handleCardPress}
      />
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>Memory Game</Text>
      </View>
      <View style={styles.infoContainer}>
        <View style={styles.infoItem}>
          <Text style={styles.infoLabel}>Attempts:</Text>
          <Text style={styles.infoValue}>{attempts}</Text>
        </View>
        <View style={styles.infoItem}>
          <Text style={styles.infoLabel}>Matches:</Text>
          <Text style={styles.infoValue}>{matches}</Text>
        </View>
      </View>

      <FlatList
        data={cards}
        renderItem={renderCardItem}
        numColumns={4}
        keyExtractor={item => item.id}
        contentContainerStyle={styles.cardsContainer}
      />

      <View style={styles.resetButtonContainer}>
        <TouchableOpacity onPress={handleResetPress} style={styles.resetButton}>
          <Text style={styles.resetButtonText}>Reset Game</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    height: 80,
    backgroundColor: '#2196f3',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
  },
  infoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  infoItem: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  infoLabel: {
    fontSize: 18,
    fontWeight: 'bold',
    marginRight: 5,
  },
  infoValue: {
    fontSize: 18,
  },
  cardsContainer: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20,
  },
  card: {
    width: 60,
    height: 80,
    backgroundColor: '#fff',
    marginHorizontal: 5,
    marginVertical: 10,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardText: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  resetButtonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 20,
  },
  resetButton: {
    backgroundColor: '#4caf50',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
  },
  resetButtonText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold',
  },
});
export default App;
